`default_nettype none
`timescale 1 ns / 1 ps 
module top(
	input wire i_pushbutton,
	output wire [7:0] o_leds
);

wire fpga_clock;
wire signed [6:0] o_sin;
wire clk_100;

/* Purpose: This project generates a 1 mhz sine wave. Uses 16 bits phase, 7 bit output
A table of 256 sine wave valeus are in table.hex
The wire named  o_sin is what contains the output data

Output values are signed, ranging from -64 to 63*/

OSCH #(.NOM_FREQ("133.00")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));
pll pll_133 (.CLKI(fpga_clock), .CLKOP(clk_100));
led_toggle led_0 (.clk(clk_100), .o_led(o_leds[0]));
dds_test dds_dut (.clk(clk_100), .sinewave(o_sin));
endmodule

module led_toggle(
	input wire clk,
	output reg o_led = 1
);
	reg [31:0] sec_cnt = 0;
	always@(posedge clk) begin
		sec_cnt <= sec_cnt + 1;
		if(sec_cnt == 100000000 - 1) begin
			o_led <= ~o_led;
			sec_cnt <= 0;
		end
	end
endmodule

module dds_test(
	input wire clk,
	output reg signed [6:0] sinewave
	);

parameter frequency_step = 655;
	reg [15:0]	phase = 0;
	reg [6:0] Table [0:255];
	initial $readmemh("table.hex", Table);
	
	always @(posedge clk) begin
		phase <= phase + frequency_step;
	end

	always @(posedge clk) begin
		sinewave <= Table[phase[15:8]];
	end
	
	
endmodule