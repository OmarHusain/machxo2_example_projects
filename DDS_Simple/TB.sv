`include "Top.sv"
`default_nettype none 
`timescale 1 ns / 1 ps

module tb();
    reg clk = 0;
    always #5 clk = ~clk;
    reg pushbutton = 0;
    wire [7:0] leds;
	wire clk_133;

    top DUT (.i_pushbutton(pushbutton), .o_leds(leds));
    
    initial begin

    end

endmodule