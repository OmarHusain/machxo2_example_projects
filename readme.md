# Example Projects
#### These projects are created to help familiarize with the Lattice Diamond workflow.

1.  Blink_LED 
    - Blink an LED once per second. Instantiates a PLL to take a native 133MHz clock to 100MHz. 

2.  DDS Sinewave generator

    - Outputs a 7 bit output sine wave. It uses a 16 bit phase value.

    - Output sinewave values range from -64 to 63
    - Check the readme.docx + dds_out.xlsx to see the generated sinewave. (These are in the project folder)
    - sin values are in table.hex
    - Heavily based on ZipCPU's excellent guide. https://zipcpu.com/dsp/2017/07/11/simplest-sinewave-generator.html


