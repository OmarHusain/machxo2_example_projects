`default_nettype none
`timescale 1 ns / 1 ps 
module top(
	input wire i_pushbutton,
	output wire [7:0] o_leds
);

wire fpga_clock;
wire clk_100;

/* Purpose: This file is a hello world equivalent, toggles a LED once every second 
It uses a native clock of 133mhz, and then uses a PLL to drive this down to 133 mhz
The PLL was generated with the IP wizard, Tools -> IPExpress
The PLL is then instantiated below*/

OSCH #(.NOM_FREQ("133.00")) rc_oscillator(.STDBY(1'b0), .OSC(fpga_clock));
pll pll_133 (.CLKI(fpga_clock), .CLKOP(clk_100));
led_toggle led_0 (.clk(clk_100), .o_led(o_leds[0]));
endmodule

module led_toggle(
	input wire clk,
	output reg o_led = 1
);
	reg [31:0] sec_cnt = 0;
	always@(posedge clk) begin
		sec_cnt <= sec_cnt + 1;
		if(sec_cnt == 100000000 - 1) begin
			o_led <= ~o_led;
			sec_cnt <= 0;
		end
	end
endmodule

